// Mengambil paket main
package main

//Import format input atau output
import (
	//Import dari folder helper
	"booking-app/helper"
	//Import fungsi i/o yang berhungan dengan text yang disediakan oleh GO
	"fmt"
	//Import fungsi sync
	"sync"
	//Import fungsi time
	"time"
)

//Mendeklarasikan variable integer dengan nilai const
const conferenceTickets int = 50

//Deklarasi variable dan nilai
var conferenceName = "Go Conference"

//Deklarasi tipe data uint atau bilangan cacah (bil positif)
var remainingtickets uint = 50

//Deklarasi variable keyword make (channel, slice, map)
var bookings = make([]UserData, 0) // Array & Slices

//Deklarasi struct dengan type data string dan uint
type UserData struct {
	firstName       string
	lastName        string
	email           string
	numberOfTickets uint
}

//Deklarasi wait group
var wg = sync.WaitGroup{}

//Menjalankan program dari package main
func main() {

	// //Menampilkan output Type data
	// fmt.Printf("ConferenceName is %T, conferenceTIckets is %T, and remainingTIckets is %T", conferenceName, conferenceTickets, remainingtickets)

	// //Menampilkan output dari variable
	// fmt.Printf("Hello welcome to %v book aplication\n", conferenceName)
	// fmt.Printf("We have total of %v tickets and %v are still available\n", conferenceTickets, remainingtickets)

	// //Menampilkan output
	// fmt.Println("Get your tickets here to attends")

	//Memanggil fungsi greatUsers
	greatUsers()

	// //Deklarasi variable dengan type data
	// var firstName string
	// var lastName string
	// var email string
	// var numberTickets int

	// //Menampilkan output dari
	// fmt.Println("Enter your first name: ")
	// fmt.Scan(&firstName)
	// fmt.Println("Enter your last name: ")
	// fmt.Scan(&lastName)
	// fmt.Println("Enter your email: ")
	// fmt.Scan(&email)
	// fmt.Println("Enter your number ticket: ")
	// fmt.Scan(&numberTickets)

	// isValidCity := city == "Singapure" || city == "London"
	// isInvalidCity := city != "Malaysia" || city != "Indonesia"
	// isValidName := len(firstName) >= 2 && len(lastName) >= 2
	// isValidEmail := strings.Contains(email, "@")
	// isValidNumberTicket := numberTickets > 0 && uint(numberTickets) <= remainingtickets

	//Deklarasikan beberapa variable untuk memanggil fungsi diluar main
	firstName, lastName, email, numberTickets := getUserInput()
	//Deklarasikan beberapa variable untuk memanggil fungsi difolder helper
	isValidName, isValidEmail, isValidNumberTicket := helper.ValidateUserInput(firstName, lastName, email, uint(numberTickets), remainingtickets)

	// if uint(numberTickets) <= remainingtickets {

	//Mendeklarasikan kondisi isValidName, isValidEmail, isValidNumberTicket
	if isValidName && isValidEmail && isValidNumberTicket {

		//Memanggil fungsi bookTicket
		bookTicket(uint(numberTickets), lastName, firstName, email, conferenceName)

		//Penerapan wait group dengan jumlah goroutine yang sedang diproses ditambah 1
		wg.Add(1)

		//Memanggil fungsi sendTicket
		go sendTicket(uint(numberTickets), firstName, lastName, email)

		// remainingtickets = remainingtickets - uint(numberTickets)
		// // bookings[0] = firstName + " " + lastName
		// bookings = append(bookings, firstName+" "+lastName)
		// fmt.Printf("The whole array: %v\n", bookings)
		// fmt.Printf("The first value: %v\n", bookings[0])
		// fmt.Printf("Array type: %T\n", bookings)
		// fmt.Printf("Array length: %v\n", len(bookings))
		// fmt.Printf("Thank you %v %v for booking %v tickets. You will receive a confirmation email at %v\n", firstName, lastName, numberTickets, email)
		// fmt.Printf("%v tickets remaining for %v\v", remainingtickets, conferenceName)

		//Mendeklarasikan variable dan memnggil fungsi gerfirstName
		firstNames := getFirstName()
		//Menampilkan output dari fungsi firstNames
		fmt.Printf("The first names of boookings are: %v\n", firstNames)

		//Mendeklarasikan kondisi if else
		if remainingtickets == 0 {
			//Menampilkan output
			fmt.Println("Our conference is booked out. Come back next year. Thank you")
		} else {
			// fmt.Printf("We only have %v tickets remaining, so you can't book %v tickets\n", remainingtickets, numberTickets)
			// continue

			//Validasi data jika data invalid
			if !isValidName {
				fmt.Println("Firstname or last name you entered is too short")
			}
			if !isValidEmail {
				fmt.Println("Email address you entered doesn't contain @ sign")
			}
			if !isValidNumberTicket {
				fmt.Println("Number ticket you entered is invalid")
			}
		}

		//Pengkondisian bersyarat dengan switch
		city := "London"
		switch city {
		case "New York":
			//Execute code for booking ticket city
		case "Singapure", "Hongkong":
			//Execute code for booking ticket city
		case "London", "Berlin":
			//Execute code for booking ticket city
		case "Mexico City":
			//Execute code for booking ticket city
		default:
			fmt.Print("No valid city selected")
		}
	}

	//Penerapan wait grup setelah eksekusi goroutine
	wg.Wait()
}

//  encapsulet logic function
// Menjalankan program dari func greatUsers
func greatUsers() {
	//Menampilkan outpu dari func greatUsers
	fmt.Printf("Welcome to %v booking application\n", conferenceName)
	fmt.Printf("We have total of %v tickets and %v are still available\n", conferenceTickets, remainingtickets)
	fmt.Println("Get your tickets here to attends")
}

// Menjalankan program dari fucn getFirstName
func getFirstName() []string {
	//Deklarasi variable dengan type data string
	firstNames := []string{}

	//for range digunakan untuk loop struktur data yang berbeda
	//variable _ untuk menampung nilai yang tidak dipakai (keranjang sampah)
	for _, booking := range bookings {
		// names := strings.Fields(booking)
		//fungsi append untuk menambahkan irisan di akhir
		firstNames = append(firstNames, booking.firstName)
	}
	// fmt.Printf("The first names of boookings are: %v\n", firstNames)
	//Melakukan return func firstName
	return firstNames
}

// func validateUserInput(firstName string, lastName string, email string, numberTickets uint) (bool, bool, bool) {
// 	isValidName := len(firstName) >= 2 && len(lastName) >= 2
// 	isValidEmail := strings.Contains(email, "@")
// 	isValidNumberTicket := numberTickets > 0 && uint(numberTickets) <= remainingtickets

// 	return isValidName, isValidEmail, isValidNumberTicket
// }

//Menjalankan func getUserInput
func getUserInput() (string, string, string, int) {
	//deklarasi variable dengan type data string dan integer
	var firstName string
	var lastName string
	var email string
	var numberTickets int

	//ask user for their name
	//Menampilkan pernyataan sebelum user melakukan input
	fmt.Println("Enter your first name: ")
	fmt.Scan(&firstName)
	fmt.Println("Enter your last name: ")
	fmt.Scan(&lastName)
	fmt.Println("Enter your email: ")
	fmt.Scan(&email)
	fmt.Println("Enter your number ticket: ")
	fmt.Scan(&numberTickets)

	//Melakukan return untuk firstName, lastName, email, numberTickets
	return firstName, lastName, email, numberTickets
}

//Menampilkan func bookTicket
func bookTicket(numberTickets uint, firstName string, lastName string, email string, conferenceName string) {
	//Deklarasi hasil remainingtickets
	remainingtickets = remainingtickets - numberTickets

	//Deklarasi variable userData
	var userData = UserData{
		firstName:       firstName,
		lastName:        lastName,
		email:           email,
		numberOfTickets: numberTickets,
	}
	// userData["firstName"] = firstName
	// userData["lastName"] = lastName
	// userData["email"] = email
	// userData["numberOfTickets"] = strconv.FormatUint(uint64(numberTickets), 10)

	// bookings[0] = firstName + " " + lastName
	// bookings = append(bookings, firstName+" "+lastName)

	//fungsi append untuk menambahkan irisan di akhir
	bookings = append(bookings, userData)
	fmt.Printf("List of bookings is %v \n", bookings)

	fmt.Printf("Thank you %v %v for booking %v tickets. You will receive a confirmation email at %v \n", firstName, lastName, numberTickets, email)
	fmt.Printf("%v tickets remaining for %v\v", remainingtickets, conferenceName)

}

// Send Ticket
//Menjalankan program sendTicket
func sendTicket(numberTickets uint, firstName string, lastName string, email string) {
	//Memberikan jeda waktu selama 10 detik
	time.Sleep(10 * time.Second)
	//Deklarasi variable dengan nilai tampilan output dari numberTickets, firstName dan lastName
	var ticket = fmt.Sprintf("%v ticket for %v %v", numberTickets, firstName, lastName)
	//Menampilkan output
	fmt.Println("###############")
	fmt.Printf("Sending ticket:\n %v \nto email address %v\n", ticket, email)
	fmt.Println("###############")
	//Penerapan wait group pada akhir goroutine
	wg.Done()
}
