package helper

import (
	"strings"
)

func ValidateUserInput(firstName string, lastName string, email string, numberTickets uint, remainingTickets uint) (bool, bool, bool) {
	isValidName := len(firstName) >= 2 && len(lastName) >= 2
	isValidEmail := strings.Contains(email, "@")
	isValidNumberTicket := numberTickets > 0 && uint(numberTickets) <= remainingTickets

	return isValidName, isValidEmail, isValidNumberTicket
}
